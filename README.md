# README #

Simple lap timer for Parkrun.

### What is this repository for? ###

App was created as a backup to normal timers in case timers are missing/out of battery/not working. Laps are exported in format that RJH Stopwatch Download app can import. User can send data via email or clipboard.

### Features ###

* Start / stop / restart / reset timer
* Record laps with lap button
* Send laps via email or from the clipboard

### Other features ###

* Long press Start/Lap (i.e. lower) button  to clear the latest lap
* Long press Pause/Email/Reset (i.e. top) button to zero timer

### Implementation ###

* App was developed with Unity 

### Who do I talk to in case of issues? ###

* ovirta (strom@iki.fi)
