﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using UnityEngine.UI;
using System.Threading;

public delegate void ChangedEventHandler(object sender, EventArgs e);

public class TimerManager : Singleton<TimerManager>
{
    public Text LapTimePrefab;
    public GameObject LapTimeContainer;
    public GameObject PauseStopButton;

    public enum States { Stopped, Running, Paused, Reset, Emailed, Lap }
    public States State = States.Reset;

    public AudioClip BeepClip;
    AudioSource _audioSource;

    public Stopwatch MyStopWatch = new Stopwatch();

    public DateTime MyDateTime = DateTime.MinValue;
    public DateTime MyStopTime = DateTime.MinValue;

    public event ChangedEventHandler Changed;

    AndroidJavaObject currentActivity;
    AndroidJavaClass UnityPlayer;
    AndroidJavaObject context;

    protected virtual void OnChanged(EventArgs e)
    {
        if (Changed != null)
            Changed(this, e);
    }

    // TODO: use this
    // List<DateTime> LapTimes = new List<DateTime>();

    void Start()
    {
        _audioSource = gameObject.AddComponent<AudioSource>();

        if (PlayerPrefs.HasKey("count"))
        {
            LoadLaps();
            State = States.Running;
            OnChanged(EventArgs.Empty);
        }
        else
        {
            State = States.Reset;
        }
    }


#if UNITY_EDITOR
    void OnApplicationQuit()
    { 
#else
    void OnApplicationPause(bool pause)
    {
        if (pause)
#endif
        {
            if (State == States.Running)
            {
                PlayerPrefs.DeleteAll();
                List<string> saveLaps = GetLapsList();
                PlayerPrefs.SetInt("count", saveLaps.Count);
                PlayerPrefs.SetString("time", MyDateTime.ToString());
                for (int i = 0; i < saveLaps.Count; i++)
                    PlayerPrefs.SetString(i.ToString(), saveLaps[i]);
                PlayerPrefs.Save();
            }
            else
                PlayerPrefs.DeleteAll();
        }
    }

    public List<string> GetLapsList()
    {
        List<string> laps = new List<string>();

        foreach (Transform child in LapTimeContainer.transform)
        {
            laps.Add(child.GetComponent<Text>().text);
        }
        return laps;
    }

    public string GetLaps()
    {
        string value = string.Empty;

        foreach (Transform child in LapTimeContainer.transform)
        {
            // input format : 1. 01:01:01.01
            // output format: 01:01:01.00
            // note: ms not processed!
            string temp = child.gameObject.GetComponent<Text>().text;
            string[] tempArray = temp.Split(' ', ':', '.');
            float f = float.Parse(tempArray[4] + "." + tempArray[5]);
            string roundedSecond = Mathf.RoundToInt(f).ToString("00");
            temp = tempArray[2] + ":" + tempArray[3] + ":" + roundedSecond + "\r\n";
            value = temp + value;
        }
        return value;
    }

    public void AddLap()
    {
        State = States.Lap;
        PopulateContainer((LapTimeContainer.transform.childCount + 1) + ". " + GetTimeHourMinSecMs());
        OnChanged(EventArgs.Empty);
    }

    void PopulateContainer(string lapString, bool skipAnimation = false)
    { 
        Text temp = Instantiate(LapTimePrefab, LapTimeContainer.transform) as Text;
        if (skipAnimation)
            temp.GetComponent<LapTextAnimationScript>().SkipAnimation = true;
        temp.transform.SetAsFirstSibling();
        temp.text = lapString;
        LapContainerDefaultPosition();
    }

    public void RemoveLap()
    {
        if (LapTimeContainer.transform.childCount > 0)
        {
            var temp = LapTimeContainer.transform.GetChild(0);
            temp.gameObject.GetComponent<LapTextDestroyScript>().DestroyLapText();
        }
    }

    public void LapContainerDefaultPosition()
    {
        LapTimeContainer.transform.localPosition = new Vector2(LapTimeContainer.transform.localPosition.x, 0f);
    }

    public bool Running()
    {
        return MyDateTime != DateTime.MinValue && MyStopTime == DateTime.MinValue;
    }

    public void StartTimer()
    {
        State = States.Running;
        MyStopWatch.Start();

        if (MyDateTime == DateTime.MinValue)
            MyDateTime = DateTime.Now;
        else
            MyDateTime += (DateTime.Now - MyStopTime);
        MyStopTime = DateTime.MinValue;
        OnChanged(EventArgs.Empty);
    }

    public void ResetTimer()
    {
        State = States.Reset;
        MyStopWatch.Reset();
        MyDateTime = DateTime.MinValue;
        MyStopTime = DateTime.MinValue;
        DeleteLaps();
        OnChanged(EventArgs.Empty);
    }

    public void StopTimer()
    {
        State = States.Stopped;
        MyStopWatch.Stop();
        MyStopTime = DateTime.Now;
        OnChanged(EventArgs.Empty);
    }

    public void EmailData()
    {
        State = States.Emailed;
        StartCoroutine(GetLapsAndProcessData());
        OnChanged(EventArgs.Empty);
    }


    IEnumerator ShowToastCoroutine()
    {
        print("show toast");
        UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        context = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
        currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(showToast));
        yield break;
    }

    void showToast()
    {
        AndroidJavaClass  Toast = new AndroidJavaClass("android.widget.Toast");
        AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject>("makeText", context, "Laps copied to clipboard!\n     Starting email app.", 0);
        toast.Call("setGravity", 17, 0, 0); // Gravity.CENTER 17
        toast.Call("show");
    }

    IEnumerator GetLapsAndProcessData()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            yield return StartCoroutine(ShowToastCoroutine());

            float time = 0f;
            while ((time += Time.deltaTime) < 2.5f)
                yield return null;
        }
        string laps = TimerManager.Instance.GetLaps();
        SendMailCrossPlatform.SendEmail("recipient", "Times: " + DateTime.Now.ToString() + " " + GetTimeHourMinSec(), laps);
        GUIUtility.systemCopyBuffer = laps;
    }

    public void TriggerSound()
    {
        _audioSource.PlayOneShot(BeepClip);
    }

    public void TriggerHaptic(long time)
    {
        Vibration.Vibrate(time);
    }

    public string GetTimeHourMinSec()
    {
        string value = string.Format("{0:HH:mm:ss}", new DateTime((DateTime.Now - MyDateTime).Ticks));
        return value;
    }

    public string GetTimeHourMinSecMs()
    {
        string value = string.Format("{0:HH:mm:ss.ff}", new System.DateTime((DateTime.Now - MyDateTime).Ticks));
        return value;
    }

    public string GetResetTime()
    {
        return "00:00:00";
    }

    void DeleteLaps()
    {
        foreach (Transform child in LapTimeContainer.transform)
            Destroy(child.gameObject);
    }

    void LoadLaps()
    {
        DeleteLaps();
        int count = PlayerPrefs.GetInt("count");
        for (int i = count - 1; i >= 0; i--)
        {
            string lapString = PlayerPrefs.GetString(i.ToString());
            PopulateContainer(lapString, true);
        }
        string time = PlayerPrefs.GetString("time");
        MyDateTime = DateTime.Parse(time);
        PlayerPrefs.DeleteAll();

    }
}
