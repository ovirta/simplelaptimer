﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SendEmail : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        SendMailCrossPlatform.SendEmail("strom@iki.fi", "Tiimes: " + DateTime.Now, GameObject.FindWithTag("TextLaps").GetComponent<Text>().text);
    }

}
