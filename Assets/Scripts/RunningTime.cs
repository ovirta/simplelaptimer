﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RunningTime : MonoBehaviour
{
    Text _timeText;

    Vector3 _originalScale;
    bool _pop = false;

    void Start()
    {
        _timeText = GetComponent<Text>();
        TimerManager.Instance.Changed += new ChangedEventHandler(StateChanged);
        InvokeRepeating("UpdateTimeText", 0f, .5f);  
    }


    private void StateChanged(object sender, EventArgs e)
    {
        HandleStatus();
    }

    private void HandleStatus()
    {
        if (TimerManager.Instance.State == TimerManager.States.Reset)
            _timeText.text = TimerManager.Instance.GetResetTime();
        else if (TimerManager.Instance.State == TimerManager.States.Lap)
        {
            TimerManager.Instance.State = TimerManager.States.Running;
            //StopCoroutine(Pop());
            //StartCoroutine(Pop());
        }

        if (TimerManager.Instance.State == TimerManager.States.Stopped)
        {
            StartCoroutine(SmoothPop());
        }
        else
        {
            _pop = false;
        }
    }

    void UpdateTimeText()
    {
        if(TimerManager.Instance.Running())
            _timeText.text = TimerManager.Instance.GetTimeHourMinSec();
    }

    IEnumerator SmoothPop()
    {
        Vector2 originalScale = transform.localScale;
        float t = 0f;
        _pop = true;
        while (_pop)
        {
            float value = Mathf.SmoothStep(0f, 1f, Mathf.PingPong((t += Time.deltaTime) / 5, 0.3f));
            transform.localScale = new Vector2(originalScale.x - value, originalScale.y - value);
            yield return null;
        }
        transform.localScale = originalScale;
    }

    IEnumerator Pop()
    {
        Vector2 _originalScale = _timeText.transform.localScale;

        float time = 0f;
        float length = .2f;

        while(time < length)
        {
            _timeText.transform.localScale = new Vector3(_originalScale.x + time, _timeText.transform.localScale.y, _timeText.transform.localScale.z);
            time += Time.deltaTime;
            yield return null;
        }
        _timeText.transform.localScale = _originalScale;

        yield break;
    }
}
