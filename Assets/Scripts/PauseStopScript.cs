﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class PauseStopScript : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
{
    public GameObject Pause;
    public GameObject Stop;
    public GameObject Email;

    bool _reset;

    void Start()
    {
        TimerManager.Instance.Changed += new ChangedEventHandler(StateChanged);
    }

    private void StateChanged(object sender, EventArgs e)
    {
        HandleStatus();
    }

    private void HandleStatus()
    {
        if (TimerManager.Instance.Running())
        {
            Pause.SetActive(true);
            Stop.SetActive(false);
            Email.SetActive(false);
        }
        else if (TimerManager.Instance.State == TimerManager.States.Stopped)
        {
            Email.SetActive(true);
            Stop.SetActive(false);
            Pause.SetActive(false);
        }
        else if (TimerManager.Instance.State == TimerManager.States.Emailed)
        {
            Email.SetActive(false);
            Pause.SetActive(false);
            Stop.SetActive(true);
        }
        else if (TimerManager.Instance.State == TimerManager.States.Reset)
        {
            Email.SetActive(false);
            Pause.SetActive(false);
            Stop.SetActive(false);
        }
        else
        {
            print(TimerManager.Instance.State);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _reset = false;

        TimerManager.Instance.TriggerSound();

        if (TimerManager.Instance.State == TimerManager.States.Reset)
            return;

        StartCoroutine(LongPress());
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        StopAllCoroutines();
        transform.localScale = new Vector3(1f, 1f, 1f);
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        if (_reset) return;

        if (TimerManager.Instance.State == TimerManager.States.Running)
            TimerManager.Instance.StopTimer();
        else if (TimerManager.Instance.State == TimerManager.States.Stopped)
            TimerManager.Instance.EmailData();
        else if (TimerManager.Instance.State == TimerManager.States.Emailed)
            TimerManager.Instance.ResetTimer();
    }

    IEnumerator LongPress()
    {
        float time = 0f;
        float length = 2f;
        float waitTime = .5f;

        while ((time += Time.deltaTime) < waitTime)
            yield return null;

        _reset = true;
        time = 0f;

        while ((time += Time.deltaTime) < length)
        {
            transform.localScale = new Vector3(1f + time, 1f + time, 1f);
            yield return null;
        }

        TimerManager.Instance.TriggerHaptic(100);
        TimerManager.Instance.ResetTimer();
        transform.localScale = new Vector3(1f, 1f, 1f);

        yield break;
    }
}
