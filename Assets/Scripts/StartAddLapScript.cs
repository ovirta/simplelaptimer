﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StartAddLapScript : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
{
    public GameObject Lap;
    public GameObject Play;

    bool _lapRemoved = false;

    void Start()
    {
        TimerManager.Instance.Changed += new ChangedEventHandler(StateChanged);
    }

    private void StateChanged(object sender, EventArgs e)
    {
        HandleStatus();
    }

    private void HandleStatus()
    {
        if (TimerManager.Instance.Running())
        {
            Play.SetActive(false);
            Lap.SetActive(true);
        }
        else if (TimerManager.Instance.State == TimerManager.States.Emailed)
        {
            Play.SetActive(false);
            Lap.SetActive(false);
        }
        else
        { 
            Play.SetActive(true);
            Lap.SetActive(false);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        TimerManager.Instance.TriggerSound();

        _lapRemoved = false;

        if (TimerManager.Instance.Running())
            StartCoroutine(LongPress());
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        StopAllCoroutines();
        Lap.GetComponent<Image>().fillAmount = 1f;
    }

    public void OnPointerClick(PointerEventData eventData)
    { 
        if (_lapRemoved) return;

        if (TimerManager.Instance.Running())
            TimerManager.Instance.AddLap();
        else if (TimerManager.Instance.State == TimerManager.States.Reset ||
                    TimerManager.Instance.State == TimerManager.States.Stopped)
            TimerManager.Instance.StartTimer();
    }

    IEnumerator LongPress()
    {
        // 1s press removes lap
        float time = 0f;
        float length = 1f;
        float waitTime = .3f;
        Image image = Lap.GetComponent<Image>();

        while ((time += Time.deltaTime) < waitTime)
            yield return null;

        _lapRemoved = true;
        time = 0f;

        while (time < length)
        {
            time += Time.deltaTime;
            image.fillAmount = 1f - (time / length);
            yield return null;
        }
        TimerManager.Instance.TriggerHaptic(100);
        TimerManager.Instance.RemoveLap();
        image.fillAmount = 1f;

        yield break;
    }
}
