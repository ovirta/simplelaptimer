﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LapTextDestroyScript : MonoBehaviour {

    // Use this for initialization
    public void DestroyLapText()
    {
        StartCoroutine(DestroyLap());
    }

    IEnumerator DestroyLap()
    {
        float time = 0f;
        float length = .5f;
        Vector2 startScale = transform.localScale;

        while((time += Time.deltaTime) < length)
        {
            transform.localScale = new Vector2(startScale.x - time, transform.localScale.y);
            yield return null;
        }
        TimerManager.Instance.LapContainerDefaultPosition();
        Destroy(transform.gameObject);

        yield break;
    }
}

