﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LapTextAnimationScript : MonoBehaviour
{
    public bool SkipAnimation { get; set; }

    void Start()
    {
        if(!SkipAnimation)
           StartCoroutine(LapAnimation());
    }

    IEnumerator LapAnimation()
    {
        float time = 0f;
        float length = 1f;
        float scale = .5f;

        Vector3 originalScale = transform.localScale;

        while((time += Time.deltaTime) < length)
        {
            transform.localScale = new Vector3(originalScale.x + scale - (scale * time/length), originalScale.y, originalScale.z);
            yield return null;
        }

        transform.localScale = originalScale;
        yield break;

    }
}