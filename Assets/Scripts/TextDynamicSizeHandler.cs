﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
[ExecuteInEditMode]
public class TextDynamicSizeHandler : MonoBehaviour
{
    [Header("Reference size and screen width")]
    public int _defaultFontSize;
    public int _defaultWidth;

    int _currentWidth;
    Text _text;

    void Start ()
    {
        if(_defaultFontSize == 0)
            _defaultFontSize = 58;
        if(_defaultWidth == 0)
            _defaultWidth = 1440;

        _currentWidth = Camera.main.pixelWidth;

        _text = GetComponent<Text>();

        _text.fontSize = _defaultFontSize * Camera.main.pixelWidth / _defaultWidth;
    }

    // Update is called once per frame
    void Update ()
    {
        if (Camera.main.pixelWidth != _currentWidth)
        {
            if (_text == null) _text = GetComponent<Text>();

            _text.fontSize = _defaultFontSize * Camera.main.pixelWidth / _defaultWidth;

            _currentWidth = Camera.main.pixelWidth;
        }
    }
}
