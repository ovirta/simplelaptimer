﻿using UnityEngine;

/// <summary>
/// - opens the mail app on android, ios & windows (tested on windows)
/// (http://answers.unity3d.com/questions/61669/applicationopenurl-for-email-on-mobile.html)
/// </summary>
public class SendMailCrossPlatform
{

    /// <param name="email"> myMail@something.com</param>
    /// <param name="subject">my subject</param>
    /// <param name="body">my body</param>
    public static void SendEmail(string email, string subject, string body)
    {
        subject = MyEscapeURL(subject);
        body = MyEscapeURL(body);
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }
    public static string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }
}